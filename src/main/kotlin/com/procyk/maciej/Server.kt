package com.procyk.maciej

import com.procyk.maciej.extractor.ExtractResults
import com.procyk.maciej.extractor.Ole
import com.procyk.maciej.html.BaseHTMLPage
import com.procyk.maciej.html.SearchPage
import com.procyk.maciej.plot.LineScatterPlotter
import com.procyk.maciej.plot.PlotData
import com.procyk.maciej.plot.plotFromScriptData
import com.procyk.maciej.server.Constants.ADD_FORM
import com.procyk.maciej.server.Constants.URL_FIELD_NAME
import com.procyk.maciej.server.installFreeMarker
import io.ktor.server.netty.*
import io.ktor.routing.*
import io.ktor.application.*
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.html.respondHtml
import io.ktor.http.content.PartData
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.request.isMultipart
import io.ktor.request.receiveMultipart
import io.ktor.response.*
import io.ktor.server.engine.*
import kotlinx.html.*

object Server {

    @JvmStatic
    fun main(args: Array<String>) {

        val plotter = LineScatterPlotter()

        embeddedServer(Netty, getEnvPort()) {
            installFreeMarker()

            routing {
                listOf("css", "img", "js", "scss", "src.js").forEach { folder ->
                    static(folder) { resources(folder) }
                }
                post(ADD_FORM) {
                    val multipart = call.receiveMultipart()

                    if (!call.request.isMultipart()) {
                        call.respondRedirect(permanent = true, url = "/")
                        return@post
                    }
                    while (true) {
                        val part = multipart.readPart() ?: break
                        if (part is PartData.FormItem && part.name == URL_FIELD_NAME) {
                            val url = part.value
                            val data = testData(url)
                            val plotData = testPlotData(data)
                            val file = plotter.generateHtmlPlotFile("Ole", plotData)
                            val scriptData = plotter.extractHtmlPlotData(file)
                            val (headPlot, bodyPlot) = plotFromScriptData(scriptData)
                            call.respondHtml(block = object : BaseHTMLPage() {
                                override val body: (BODY) -> Unit
                                    get() = bodyPlot
                                override val head: (HEAD) -> Unit
                                    get() = headPlot
                            }.generatePage("Your plot data"))
                        }
                        part.dispose()
                    }

                }
                get("/") {
                    call.respondHtml(block = SearchPage.generatePage())
                }
                get("/list") {
                    val data = testData("https://www.oleole.pl/laptopy-i-netbooki,_Dell:MSI,dysk-ssd!256-gb:128-gb:32-gb:6:7:8:10:15:17.bhtml")
                    call.respond(FreeMarkerContent("list.ftl", mapOf("data" to data.pricesData)))
                }
            }
        }.start(wait = true)
    }

    private fun testData(urlGiven: String): ExtractResults {
        var url = urlGiven
        val result = mutableListOf<ExtractResults>()
        while (true) {
            val currResults = Ole.extractPrices(url)
            if (currResults.size == 0) {
                break
            }
            result += currResults
            url = Ole.getNextPageUrl(url)
        }
        var data = ExtractResults()
        for (singleResult in result) {
            data = data.combineWith(singleResult)
        }
        return data
    }

    private fun testPlotData(result: ExtractResults): List<PlotData> {
        val names = List(result.size) { it.toString() }
        val values = result.pricesData.map { it.price }
        val returnList = mutableListOf<PlotData>()
        repeat(5) { idx ->
            returnList += PlotData(names, values.map { it + 100 * (1..20).random() }, idx.toString())
        }
        return returnList
    }

    private const val DEFAULT_PORT = 7000

    private fun getEnvPort(): Int {
        val herokuPort = System.getenv("PORT")
        return herokuPort?.toInt() ?: DEFAULT_PORT
    }
}



