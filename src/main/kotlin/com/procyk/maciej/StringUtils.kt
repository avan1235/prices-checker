package com.procyk.maciej

fun toLastSpace(data: String): String = data.substring(0, data.lastIndexOf(' '))

fun removeSpaces(data: String): String = data.replace("\\s+".toRegex(), "")

fun toDouble(data: String): Double = data.toDouble()