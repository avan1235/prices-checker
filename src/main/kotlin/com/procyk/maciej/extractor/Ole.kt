package com.procyk.maciej.extractor

import com.procyk.maciej.server.generateUserAgent
import com.procyk.maciej.removeSpaces
import com.procyk.maciej.toDouble
import com.procyk.maciej.toLastSpace
import it.skrape.core.fetcher.Mode
import it.skrape.core.htmlDocument
import it.skrape.extract
import it.skrape.selects.and
import it.skrape.selects.Doc
import it.skrape.selects.eachText
import it.skrape.selects.html5.div
import it.skrape.selects.html5.h2
import it.skrape.skrape
import java.io.IOException

object Ole : PricesExtractor, NextPageGenerator {

    override fun extractPrices(specifiedUrl: String): ExtractResults {
        var names: List<String> = listOf()
        var foundPrices: List<String> = listOf()
        var ids: List<String> = listOf()
        try {
            skrape {
                url = specifiedUrl
                userAgent = generateUserAgent()
                mode = Mode.SOURCE
                extract {
                    htmlDocument {
                        foundPrices = div {
                            withClass = "price-normal" and "selenium-price-normal"
                            findAll { eachText() }}
                        names = h2 {
                            withClass = "product-name"
                            findAll { eachText() }
                        }
                        ids = div {
                            withClass = "selenium-product-code"
                            findAll { eachText() }.map { it.takeWhile { it != ' ' } }
                        }
                    }
                }
            }
            val values = foundPrices.map(::toLastSpace).map(::removeSpaces).map(::toDouble)
            return ExtractResults(ids, names, values)
        } catch (e: IOException) {
            return ExtractResults()
        } catch (e: Exception) {
            // TODO some logging here
            return ExtractResults()
        }
    }

    override fun getNextPageUrl(currentPageUrl: String): String {
        val token = "strona-"
        if (!currentPageUrl.contains(token))  {
            val begin = currentPageUrl.dropLastWhile { it != '.' }
            return "$begin,${token}2.bhtml"
        }
        val idx = currentPageUrl.indexOf(token)
        val dot = currentPageUrl.lastIndexOf('.')
        val begin = currentPageUrl.take(idx)
        val lastNumber = currentPageUrl.substring(idx + token.length, dot).toInt()
        return "$begin,$token${lastNumber + 1}.bhtml"
    }
}