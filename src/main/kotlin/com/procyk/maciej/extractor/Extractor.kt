package com.procyk.maciej.extractor

data class PricedItem(val id: String, val name: String, val price: Double)

internal fun fromPairOfPairs(data: Pair<String, Pair<String, Double>>) =
    PricedItem(data.first, data.second.first, data.second.second)

data class ExtractResults(private val ids: List<String> = listOf(),
                          private val names: List<String> = listOf(),
                          private val prices: List<Double> = listOf()) {

    val pricesData: List<PricedItem> = ids.zip(names.zip(prices)).map(::fromPairOfPairs)

    val size: Int
    get() { return pricesData.size }

    override fun toString(): String {
        val longest = pricesData.map(PricedItem::name).map(String::length).max() ?: 0
        val sb = StringBuilder()
        for ((name, price) in pricesData) {
            sb.append(name.padStart(longest)).append(" - ").append(price).append(System.lineSeparator())
        }
        return sb.toString()
    }

    fun combineWith(data: ExtractResults): ExtractResults {
        val ids = this.ids + data.ids
        val names = this.names + data.names
        val prices = this.prices + data.prices
        return ExtractResults(ids, names, prices)
    }
}

interface PricesExtractor {
    /**
     * Generate ExtractResults based on the data from specified
     * by url website. When no data is extracted from website
     * then the size of ExtractResults == 0
     */
    fun extractPrices(specifiedUrl: String): ExtractResults
}

interface NextPageGenerator {
    fun getNextPageUrl(currentPageUrl: String): String
}