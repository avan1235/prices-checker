package com.procyk.maciej.plot

import com.procyk.maciej.html.BodyContent
import com.procyk.maciej.html.HTMLDataPage
import com.procyk.maciej.html.HeadContent
import it.skrape.core.htmlDocument
import kotlinx.html.*
import scientifik.plotly.Plotly
import scientifik.plotly.makeFile
import java.io.File
import scientifik.plotly.models.Mode
import scientifik.plotly.models.Type
import scientifik.plotly.trace
import javax.management.Query.div

data class PlotData(val xLabels: List<String>, val yValues: List<Number>, val legendName: String)

typealias PlotScript = String

abstract class PlotCreator {
    abstract fun generateHtmlPlotFile(plotName: String, data: List<PlotData>): File

    fun generateHtmlPlotFile(plotName: String, vararg data: PlotData): File
            = generateHtmlPlotFile(plotName, listOf(*data))

    fun extractHtmlPlotData(file: File): PlotScript {
        val content = StringBuilder()
        file.readLines().forEach { content.append(it.trim()) }
        val idB = content.indexOf("Plotly.newPlot")
        val idE = content.indexOf("</script></body></html>")
        return content.substring(idB, idE)
    }
}

fun plotFromScriptData(scriptData: PlotScript): HTMLDataPage {
    return HTMLDataPage(
        { head: HEAD ->
            head.script { src = "https://cdn.plot.ly/plotly-latest.min.js" }
            head.link("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css", "stylesheet", "text/css")
            head.link("css/plot.css", "stylesheet", "text/css")
    }, { body: BODY ->
        body.div { id = "plot" }
        body.script { unsafe { +scriptData } }
    })
}

class LineScatterPlotter : PlotCreator() {

    override fun generateHtmlPlotFile(plotName: String, data: List<PlotData>): File {

        val plot = Plotly.plot2D {
            for (singleData in data) {
                trace {
                    x = singleData.xLabels
                    y = singleData.yValues
                    mode = Mode.`lines+markers`
                    type = Type.scatter
                }
            }
            layout {
                title = plotName
            }
        }

        val file = createTempFile()
        plot.makeFile(file, false)
        return file
    }
}