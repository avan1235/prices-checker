package com.procyk.maciej.html

import com.procyk.maciej.server.Constants
import kotlinx.html.*

interface HTMLPage {
    fun generatePage(pageTitle: String = Constants.TITLE, iconPath: String? = "img/favicon.png"): HTML.() -> Unit
}

typealias HeadContent = (HEAD) -> Unit
typealias BodyContent = (BODY) -> Unit

data class HTMLDataPage(val head: HeadContent, val body: BodyContent)

abstract class BaseHTMLPage : HTMLPage {

    abstract val body: BodyContent

    abstract val head: HeadContent

    override fun generatePage(pageTitle: String, iconPath: String?): HTML.() -> Unit = {
        head {
            meta("viewport", "width=device-width, initial-scale=1, shrink-to-fit=no", "UTF-8")
            meta {
                content = "ie=edge"
                httpEquiv = "x-ua-compatible"
            }
            title(pageTitle)
            iconPath?.let { link(it, "icon", "image/x-icon") }
            listOf("https://use.fontawesome.com/releases/v5.11.2/css/all.css", "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap").forEach { href ->
                link(href, "stylesheet")
            }
            listOf("css/bootstrap.css", "css/mdb.css", "css/style.css").forEach { href ->
                link(href, "stylesheet")
            }
            head(this)
        }
        body {
            classes = setOf("full-body")
            body(this)
            listOf("js/jquery.js", "js/popper.js", "js/bootstrap.js", "js/mdb.js").forEach { js ->
                script("text/javascript", js) { }
            }
        }
    }
}
