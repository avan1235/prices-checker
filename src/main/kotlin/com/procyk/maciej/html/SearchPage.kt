package com.procyk.maciej.html

import com.procyk.maciej.server.Constants.ADD_FORM
import com.procyk.maciej.server.Constants.URL_FIELD_NAME
import kotlinx.html.*

object SearchPage : BaseHTMLPage() {
    override val body: (BODY) -> Unit
        get() = {
            with(it) {
                form(ADD_FORM,
                    encType = FormEncType.multipartFormData,
                    method = FormMethod.post) {
                    acceptCharset = "utf-8"
                    classes = setOf("flex-container", "center-flex")
                    div {
                        classes = setOf("flex-row")
                        div {
                            classes = setOf("flex-item")
                            div {
                                classes = setOf("form-inline", "d-flex", "justify-content-center", "md-form", "active-pink", "active-pink-2", "mt-2")
                                i {
                                    classes = setOf("fas", "fa-search")
                                    attributes["aria-hidden"] = "true"
                                }
                                this@form.textInput {
                                    classes = setOf("form-control", "form-control-sm", "ml-3", "w-75")
                                    name = URL_FIELD_NAME
                                }
                            }
                        }
                        div {
                            classes = setOf("flex-item")
                            this@form.submitInput {
                                classes = setOf("btn", "btn-indigo", "custom-button")
                                value = "Follow items"
                            }
                        }
                    }
                }
            }
        }
    override val head: (HEAD) -> Unit
        get() = { }

}